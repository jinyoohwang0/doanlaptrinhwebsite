﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace DACSWebBanFlower.Migrations
{
    /// <inheritdoc />
    public partial class AddnewTLoaiSp : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TDanhMucSps_TLoaiSps_TLoaiSpId",
                table: "TDanhMucSps");

            migrationBuilder.RenameColumn(
                name: "Id",
                table: "TLoaiSps",
                newName: "MaLoai");

            migrationBuilder.RenameColumn(
                name: "TLoaiSpId",
                table: "TDanhMucSps",
                newName: "TLoaiSpMaLoai");

            migrationBuilder.RenameIndex(
                name: "IX_TDanhMucSps_TLoaiSpId",
                table: "TDanhMucSps",
                newName: "IX_TDanhMucSps_TLoaiSpMaLoai");

            migrationBuilder.AddForeignKey(
                name: "FK_TDanhMucSps_TLoaiSps_TLoaiSpMaLoai",
                table: "TDanhMucSps",
                column: "TLoaiSpMaLoai",
                principalTable: "TLoaiSps",
                principalColumn: "MaLoai");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TDanhMucSps_TLoaiSps_TLoaiSpMaLoai",
                table: "TDanhMucSps");

            migrationBuilder.RenameColumn(
                name: "MaLoai",
                table: "TLoaiSps",
                newName: "Id");

            migrationBuilder.RenameColumn(
                name: "TLoaiSpMaLoai",
                table: "TDanhMucSps",
                newName: "TLoaiSpId");

            migrationBuilder.RenameIndex(
                name: "IX_TDanhMucSps_TLoaiSpMaLoai",
                table: "TDanhMucSps",
                newName: "IX_TDanhMucSps_TLoaiSpId");

            migrationBuilder.AddForeignKey(
                name: "FK_TDanhMucSps_TLoaiSps_TLoaiSpId",
                table: "TDanhMucSps",
                column: "TLoaiSpId",
                principalTable: "TLoaiSps",
                principalColumn: "Id");
        }
    }
}
