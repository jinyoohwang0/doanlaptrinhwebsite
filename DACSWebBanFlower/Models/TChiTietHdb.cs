﻿namespace DACSWebBanFlower.Models
{
    public class TChiTietHdb
    {
        public string Id { get; set; } = null!;

        public string ChiTietSanPhamId { get; set; } = null!;

        public int? SoLuongBan { get; set; }

        public decimal? DonGiaBan { get; set; }

        public double? GiamGia { get; set; }

        public string? GhiChu { get; set; }

        public TChiTietSanPham? TChiTietSanPham{ get; set; } 

        public THoaDonBan? THoaDonBan { get; set; } 
    }
}
