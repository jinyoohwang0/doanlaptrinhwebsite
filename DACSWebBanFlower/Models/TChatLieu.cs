﻿namespace DACSWebBanFlower.Models
{
    public class TChatLieu
    {
        public string Id { get; set; } = null!;

        public string? ChatLieu { get; set; }

        public virtual ICollection<TDanhMucSp> TDanhMucSps { get; set; } = new List<TDanhMucSp>();
    }
}
