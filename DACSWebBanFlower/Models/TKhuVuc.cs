﻿namespace DACSWebBanFlower.Models
{
    public class TKhuVuc
    {
        public string Id { get; set; } = null!;

        public string? TenKhuVuc { get; set; }

        public virtual ICollection<TDanhMucSp> TDanhMucSps { get; set; } = new List<TDanhMucSp>();
    }
}
