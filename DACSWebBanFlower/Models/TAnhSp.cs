﻿namespace DACSWebBanFlower.Models
{
    public class TAnhSp
    {
        public string Id { get; set; } = null!;

        public string TenFileAnh { get; set; } = null!;

        public short? ViTri { get; set; }   

        public TDanhMucSp? DanhMucSpId { get; set; } = null!;
    }
}
