﻿namespace DACSWebBanFlower.Models
{
    public class THoaDonBan
    {
        public string Id { get; set; } = null!;

        public DateTime? NgayHoaDon { get; set; }

        public string? KhachHangId { get; set; }

        public string? NhanVienId { get; set; }

        public decimal? TongTienHd { get; set; }

        public double? GiamGiaHd { get; set; }

        public byte? PhuongThucThanhToan { get; set; }

        public string? MaSoThue { get; set; }

        public string? ThongTinThue { get; set; }

        public string? GhiChu { get; set; }

        public TKhachHang? TKhachHang { get; set; }

        public TNhanVien? TNhanVien { get; set; }

        public virtual ICollection<TChiTietHdb> TChiTietHdbs { get; set; } = new List<TChiTietHdb>();
    }
}
