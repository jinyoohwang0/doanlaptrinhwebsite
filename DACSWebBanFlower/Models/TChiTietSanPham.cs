﻿namespace DACSWebBanFlower.Models
{
    public class TChiTietSanPham
    {
        public string Id { get; set; } = null!;

        public string? DanhMucSpId { get; set; }

        public string? KichThuocId { get; set; }

        public string? MauSacId { get; set; }

        public string? AnhDaiDien { get; set; }

        public string? Video { get; set; }

        public double? DonGiaBan { get; set; }

        public double? GiamGia { get; set; }

        public int? Slton { get; set; }

        public virtual TKichThuoc? TKichThuoc { get; set; }

        public virtual TMauSac? TMauSac { get; set; }

        public virtual TDanhMucSp? TDanhMucSp { get; set; }

        public virtual ICollection<TAnhChiTietSp> TAnhChiTietSps { get; set; } = new List<TAnhChiTietSp>();

        public virtual ICollection<TChiTietHdb> TChiTietHdbs { get; set; } = new List<TChiTietHdb>();
    }
}
