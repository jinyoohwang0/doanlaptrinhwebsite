﻿using DACSWebBanFlower.Models;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using System.Security.Cryptography;
using DACSWebBanFlower.Models.Authentication;

namespace DACSWebBanFlower.Controllers
{
    public class AccessController : Controller
    {
        private readonly QlbanFlowerContext _db;
        private TUser AuthenticateUser(string username, string password)
        {
            // Truy vấn cơ sở dữ liệu để lấy thông tin người dùng dựa trên tên đăng nhập và mật khẩu
            var user = _db.TUsers.FirstOrDefault(u => u.Id == username && u.Password == password);
            return user;
        }
        public AccessController(QlbanFlowerContext db)
        {
            _db = db;
        }

        //Login form
        [HttpGet]
        public IActionResult Login()
        {
            // Kiểm tra xem người dùng đã đăng nhập chưa
            if (HttpContext.Session.GetString("Id") != null)
            {
                // Nếu đã đăng nhập, chuyển hướng đến trang "Index"
                return RedirectToAction("Index", "Home");
            }

            // Nếu chưa đăng nhập, hiển thị trang Index chứa thông tin "Login (chưa đăng nhập)"
            return View();
        }
        [HttpPost]
        public IActionResult Login(TUser user)
        {
            if (ModelState.IsValid)
            {
                // Kiểm tra xác thực người dùng
                var authenticatedUser = AuthenticateUser(user.Id, user.Password);

                if (authenticatedUser != null)
                {
                    // Kiểm tra quyền truy cập của người dùng và điều hướng đến trang tương ứng
                    if (authenticatedUser.LoaiUser == 1) // Quyền admin
                    {
                        // Điều hướng đến trang admin
                        return RedirectToAction("Index", "Admin", new { area = "Admin" });
                    }
                    else if (authenticatedUser.LoaiUser == 2) // Quyền khách hàng
                    {
                        // Đăng nhập thành công, thiết lập session và chuyển hướng đến trang ShopIndex
                        HttpContext.Session.SetString("Id", authenticatedUser.Id);
                        // Điều hướng đến trang chủ
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    // Đăng nhập thất bại, hiển thị trang đăng nhập với thông báo lỗi
                    ModelState.AddModelError(string.Empty, "Invalid username or password.");
                    return View();
                }
            }

            // Dữ liệu đầu vào không hợp lệ, hiển thị lại trang đăng nhập
            return View(user);
        }
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();

            // Lấy trang trước đó từ session
            string? previousUrl = HttpContext.Session.GetString("PreviousUrl");

            if (!string.IsNullOrEmpty(previousUrl))
            {
                // Xóa trang trước đó từ session
                HttpContext.Session.Remove("PreviousUrl");
                return Redirect(previousUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }


        //Register form
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Register(TUser user)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var existingUser = _db.TUsers.FirstOrDefault(u => u.Id == user.Id);
                    if (existingUser == null)
                    {

                        _db.TUsers.Add(user);
                        _db.SaveChanges();

                        HttpContext.Session.SetString("Id", user.Id);

                        // Hiển thị thông báo thành công
                        TempData["SuccessMessage"] = "User registered successfully.";

                        return RedirectToAction("Index", "Home");
                    }
                    else
                    {
                        ModelState.AddModelError(string.Empty, "Username already exists.");
                    }
                }
                return View(user);
            }
            catch (Exception ex)
            {
                // Log lỗi để dễ dàng xác định và sửa lỗi
                // Đồng thời, bạn có thể hiển thị thông báo lỗi hoặc thực hiện xử lý khác tùy thuộc vào nhu cầu của bạn
                // Ví dụ: Log lỗi và chuyển hướng đến một trang lỗi hoặc hiển thị thông báo lỗi
                Console.WriteLine($"An error occurred: {ex.Message}");
                ModelState.AddModelError(string.Empty, "An error occurred while processing your request.");
                return View(user);
            }
        }
    }
}
