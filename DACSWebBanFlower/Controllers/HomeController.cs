using DACSWebBanFlower.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using X.PagedList;
using DACSWebBanFlower.Models;
using DACSWebBanFlower.Models.Authentication;

namespace DACSWebBanFlower.Controllers
{
    public class HomeController : Controller
    {
        QlbanFlowerContext db=new QlbanFlowerContext();
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }
/*Danh muc san pham*/
        
        public IActionResult Index(int? page)
        {
            int pageSize = 8;
            int pageNumber = page == null || page < 0 ? 1 : page.Value;
            var lstSanPham = db.TDanhMucSps.AsNoTracking().OrderBy(x => x.TenSp);
            PagedList<TDanhMucSp> lst = new PagedList<TDanhMucSp>(lstSanPham, pageNumber, pageSize);
            return View(lst);
        }
        
/*Phan san pham theo loai*/
                public IActionResult SanPhamTheoLoai(String maloai, int? page) /*Chuyen 2 tham so ma loai va page => phai chuyen 2 tham so vao SP theo loai */
                {
                    int pageSize = 8;
                    int pageNumber = page == null || page < 0 ? 1 : page.Value;
                    var lstSanPham = db.TDanhMucSps.AsNoTracking().Where(x=>x.MaLoai==maloai).OrderBy(x => x.TenSp);
                    PagedList<TDanhMucSp> lst = new PagedList<TDanhMucSp>(lstSanPham, pageNumber, pageSize);
                    ViewBag.maloai=maloai;
                    return View(lst);
                }
/*Chi tiet san pham*/
        public IActionResult ChiTietSanPham(string maSp)
        {
            var sanPham=db.TDanhMucSps.SingleOrDefault(x=>x.Id==maSp);
            var anhSanPham = db.TAnhSps.Where(x=>x.Id ==maSp).ToList();
            ViewBag.anhSanPham = anhSanPham;
            return View(sanPham);
        }
/*Dung viewmodel de tao ra productdetail tao ra xem chi tiet san pham*/
                public IActionResult ProductDetail(string maSp)
                {
                    var sanPham = db.TDanhMucSps.SingleOrDefault(x => x.Id == maSp);
                    var anhSanPham = db.TAnhSps.Where(x => x.Id == maSp).ToList();
                    var homeproductdetailviewmodel = new HomeProductDetailViewModel 
                    {
                        danhMucSp = sanPham, anhSps = anhSanPham 
                    };
                    return View(homeproductdetailviewmodel);
                }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
