﻿using DACSWebBanFlower.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using X.PagedList;

namespace DACSWebBanFlower.Areas.Admin.Controllers
{
    [Area("admin")]
    [Route("admin")]
    [Route("admin/homeadmin")]
    public class HomeAdminController : Controller
    {
        QlbanFlowerContext db = new QlbanFlowerContext();
        [Route("")]
        [Route("index")]
        public IActionResult Index()
        {
            return View();
        }

        // Action cho signout
        [HttpGet]
        [Route("Signout")]
        public IActionResult Signout()
        {
            // Xóa Session hoặc đăng xuất người dùng ở đây
            HttpContext.Session.Clear();
            // Sau đó, chuyển hướng đến trang đăng nhập
            return RedirectToAction("Login", "Access", new { area = "" });
        }
        //Danh sách sản phẩm
        [Route("danhmucsanpham")]
        public IActionResult DanhMucSanPham(int? page)
        {
            int pageSize = 20;
            int pageNumber = page == null || page < 0 ? 1 : page.Value;
            var lstSanPham = db.TDanhMucSps.AsNoTracking().OrderBy(x => x.TenSp);
            PagedList<TDanhMucSp> lst = new PagedList<TDanhMucSp>(lstSanPham, pageNumber, pageSize);
            return View(lst);
        }
        
        //Thêm mới 1 sản phẩm
        [Route("ThemSanPham")]
        [HttpGet]
        public IActionResult ThemSanPham()
        {
            ViewBag.MaChatLieu = new SelectList(db.TChatLieus.ToList(), "MaChatLieu", "ChatLieu");
            ViewBag.MaHangSx = new SelectList(db.THangSxes.ToList(), "MaHangSx", "HangSx");
            ViewBag.MaNuocSx = new SelectList(db.TKhuVucs.ToList(), "MaNuoc", "TenNuoc");
            ViewBag.MaLoai = new SelectList(db.TLoaiSps.ToList(), "MaLoai", "Loai");

            return View();
        }
        [Route("ThemSanPham")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ThemSanPham(TDanhMucSp sanPham)
        {
            if (ModelState.IsValid)
            {
                db.TDanhMucSps.Add(sanPham);
                db.SaveChanges();
                return RedirectToAction("DanhMucSanPham");
            }
            return View(sanPham);
        }
        //Chỉnh sửa sản phẩm
        [Route("SuaSanPham")]
        [HttpGet]
        public IActionResult SuaSanPham(string maSanPham)
        {
            ViewBag.MaChatLieu = new SelectList(db.TChatLieus.ToList(), "MaChatLieu", "ChatLieu");
            ViewBag.MaHangSx = new SelectList(db.THangSxes.ToList(), "MaHangSx", "HangSx");
            ViewBag.MaNuocSx = new SelectList(db.TKhuVucs.ToList(), "MaNuoc", "TenNuoc");
            ViewBag.MaLoai = new SelectList(db.TLoaiSps.ToList(), "MaLoai", "Loai");


            var sanPham = db.TDanhMucSps.Find(maSanPham);
            return View(sanPham);
        }
        [Route("SuaSanPham")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SuaSanPham(TDanhMucSp sanPham)
        {
            if (ModelState.IsValid)
            {
                db.Update(sanPham);
                db.SaveChanges();
                return RedirectToAction("DanhMucSanPham", "HomeAdmin");
            }
            return View(sanPham);
        }
// Xóa Sản Phẩm - HttpGet
        [Route("XoaSanPham")]
        [HttpGet]
        public IActionResult XoaSanPham(string maSanPham)
        {
            TempData["Message"] = "";

            // Xóa tất cả các chi tiết sản phẩm liên quan
            var chiTietSanPhams = db.TChiTietSanPhams.Where(x => x.DanhMucSpId == maSanPham);
            db.TChiTietSanPhams.RemoveRange(chiTietSanPhams);

            // Xóa hình ảnh của sản phẩm (nếu có)
            var anhSanPham = db.TAnhSps.Where(x => x.Id == maSanPham);
            if (anhSanPham.Any())
                db.RemoveRange(anhSanPham);

            // Xóa sản phẩm
            var sanPham = db.TDanhMucSps.Find(maSanPham);
            if (sanPham != null)
            {
                db.TDanhMucSps.Remove(sanPham);
                db.SaveChanges();
                TempData["Message"] = "Đã xóa sản phẩm";
            }
            else
            {
                TempData["Message"] = "Không tìm thấy sản phẩm để xóa";
            }

            return RedirectToAction("DanhMucSanPham", "HomeAdmin");
        }

        //Xóa Sản Phẩm - HttpPost
        [Route("XoaSanPham")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult XoaSanPham(TDanhMucSp sanPham)
        {
            if (ModelState.IsValid)
            {
                db.Remove(sanPham);
                db.SaveChanges();
                return RedirectToAction("DanhMucSanPham", "HomeAdmin");
            }
            return View(sanPham);
        }
//Loại Sản Phẩm 
        [Route("loaisanpham")]
        public IActionResult LoaiSanPham(int? page)
        {
            int pageSize = 20;
            int pageNumber = page == null || page < 0 ? 1 : page.Value;
            var lstLoaiSp = db.TLoaiSps.AsNoTracking().OrderBy(x => x.Loai);
            PagedList<TLoaiSp> lst = new PagedList<TLoaiSp>(lstLoaiSp, pageNumber, pageSize);
            return View(lst);
        }
        //Thêm mới 1 loại sản phẩm mới
        [Route("ThemLoaiSp")]
        [HttpGet]
        public IActionResult ThemLoaiSp()
        {
            var listLoaiSp = db.TLoaiSps.ToList();
            if (listLoaiSp != null && listLoaiSp.Any())
            {
                ViewBag.MaLoai = new SelectList(listLoaiSp, "MaLoai", "Loai");
            }
            else
            {
                TempData["ErrorMessage"] = "Không có loại sản phẩm nào có sẵn để chọn.";
            }
            return View();
        }
        [Route("ThemLoaiSp")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ThemLoaiSp(TLoaiSp loaiSp)
        {
            if (ModelState.IsValid)
            {
                db.TLoaiSps.Add(loaiSp);
                db.SaveChanges();
                return RedirectToAction("LoaiSanPham");
            }
            return View(loaiSp);
        }
        //Chỉnh sửa sản phẩm
        [Route("SuaLoaiSp")]
        [HttpGet]
        public IActionResult SuaLoaiSp(string maloaiSp)
        {
            ViewBag.MaLoai = new SelectList(db.TLoaiSps.ToList(), "MaLoai", "Loai");

            var loaiSp = db.TLoaiSps.Find(maloaiSp);
            return View(loaiSp);
        }
        [Route("SuaLoaiSp")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult SuaLoaiSp(TLoaiSp maloaiSp)
        {
            if (ModelState.IsValid)
            {
                db.Update(maloaiSp);
                db.SaveChanges();
                return RedirectToAction("LoaiSanPham", "HomeAdmin");
            }
            return View(maloaiSp);
        }
        // Xóa Sản Phẩm - HttpGet
        [Route("XoaLoaiSanPham")]
        [HttpGet]
        public IActionResult XoaLoaiSanPham(string maloaiSp)
        {
            TempData["Message"] = "";

            // Xóa loại sản phẩm
            var loaiSp = db.TLoaiSps.Find(maloaiSp);
            if (maloaiSp != null)
            {
                db.TLoaiSps.Remove(loaiSp);
                db.SaveChanges();
                TempData["Message"] = "Đã xóa loại sản phẩm";
            }
            else
            {
                TempData["Message"] = "Không tìm thấy sản phẩm để xóa";
            }

            return RedirectToAction("LoaiSanPham", "HomeAdmin");
        }

        //Xóa Sản Phẩm - HttpPost
        [Route("XoaLoaiSanPham")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult XoaLoaiSanPham(TLoaiSp maloaiSp)
        {
            if (ModelState.IsValid)
            {
                db.Remove(maloaiSp);
                db.SaveChanges();
                return RedirectToAction("LoaiSanPham", "HomeAdmin");
            }
            return View(maloaiSp);
        }
    }
}
